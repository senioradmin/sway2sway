#!/bin/sh

set -e

# user at remote (target) machine
USRTGT="myuser"
# hostname or ip of remote (target) machine
HSTTGT="myremotehost"
# keyboard layout
KB="de"
# WAYLAND_DISPLAY of target machine
WDISP="wayland-1"
# Optional Argument
ARG1="$1"

if [ "$ARG1" = "kill" ]; then
  echo "Killing connection"
  ssh -S /tmp/.sway2sway -O exit ${USRTGT}@${HSTTGT}
  exit 0
elif [ "$ARG1" = "help" ]; then
  echo "Usage: $0 [kill]"
  exit 0
fi

# create virtual output if it not exists
if ! swaymsg -pt get_outputs|grep -q HEADLESS-1;  then
  swaymsg create_output HEADLESS-1
fi

echo "Establishing connection ..."
ssh -o ControlMaster=yes -S /tmp/.sway2sway -f -L 15900:127.0.0.1:5900 ${USRTGT}@${HSTTGT} "if pidof wayvnc; then killall wayvnc; sleep 1; fi; export WAYLAND_DISPLAY=${WDISP}; wayvnc -k ${KB}" 1>/dev/null
sleep 2
echo "Connection ready, starting VNC Viewer"
gvncviewer 127.0.0.1:10000 &
sleep 1
swaymsg "[app_id=gvncviewer]" move to output HEADLESS-1
