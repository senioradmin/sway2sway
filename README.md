# sway2sway

A x2x equivalent for sway

## Requirements
* wayvnc
* gvncviewer (or another wayland compatible vnc viewer, YMMV)
* SSH server on the target machine, OpenSSH client on local host
* sway on both machines :)

## Instructions
* `git clone https://codeberg.org/senioradmin/sway2sway.git`
* install wayvnc on target machine
* install gvncviewer on local host
* edit sway2sway.sh and fill out USRTGT (username on target machine), HSTTGT (hostname or IP of target) and KB (keyboard layout)

## Usage 

### For establishing a connection
`sh sway2sway.sh`

### For ending a connection
`sh sway2sway.sh kill`

## How it works
The script creates a virtual output HEADLESS-1 and attachs it at the right. It then starts wayvnc on the target machine and forwards the VNC connection from localhost to the target machine through SSH, so that the connection is secure. It then starts gvncviewer on the local host and moves it to the virtual output. You can now move the mouse to the right border of your local host an then use the target machine.

## Issues
* Only connecting to the right (or "east" in x2x terminology) for now. You may be able to move HEADLESS-1 to other locations with appropriate tools or the `swaymsg output` command. Example:
```
# Shift HEADLESS-1 more to the right temporarily
swaymsg output HEADLESS-1 pos 3840 0

# Shift physical output to the right
swaymsg output eDP-1 pos 1920 0

# Move HEADLESS-1 to the left of physical display
swaymsg output HEADLESS-1 pos 0 0
```


* The virtual output HEADLESS-1 is not deleted when the connection ends, there is no command for removing a virtual output on the fly in sway.
